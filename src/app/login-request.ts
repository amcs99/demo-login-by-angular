export class LoginRequest {
    username: string;
    password: string;
    prefix: string;
    appCode: string;
}