import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    loginForm;
    isLogin: boolean = false;
    constructor(
        private formBuilder: FormBuilder,
        private loginService: LoginService,
        private router: Router,
    ) {
        this.loginForm = this.formBuilder.group({
            userName: '',
            passWord: ''
        });
    }
    onSubmit(customerData) {
        this.isLogin = this.loginService.checkLogin(customerData.userName, customerData.passWord);
    }
}