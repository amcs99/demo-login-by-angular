export class LoginResponse{
    sessionId: string;
    username: string;
    domain: string;
    application: ApplicationLoginResponse[];
    role: RoleLoginResponse[];
    function: FunctionLoginResponse[];
    api: string;
    token: string;
}

export class ApplicationLoginResponse{
    appCode: string;
    appName: string;
}

export class RoleLoginResponse{
    roleCode: string;
    roleName: string;
}

export class FunctionLoginResponse{
    functionCode: string;
    functionName: string;
}