import { Component } from '@angular/core';
import { LoginService } from '../login.service';

@Component({
    selector: 'app-login-result',
    templateUrl: './login-result.component.html',
    styleUrls: ['./login-result.component.css']
}
)
export class LoginResultComponent{
    isLogin: boolean = false;
    constructor(
        private loginService: LoginService
    ){
        this.isLogin = loginService.getIsLogin();
    }
    getResult(){
        if(this.isLogin){
            return 'Login Successfully!';
        }else{
            return 'Login Failed!';
        }
    }
}