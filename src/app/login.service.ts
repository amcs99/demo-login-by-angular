import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { LoginRequest } from './login-request';
import { LoginResponse } from './login-response';

@Injectable({
    providedIn: 'root'
})
export class LoginService {
    isLogin;
    private urlApi = "https://apigwmbccs.unitel.com.la:8118/ApigwGateway/CoreService/UserLogin";
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    constructor(
        private http: HttpClient,
        private ngxSpinner: NgxSpinnerService,
    ) {
        this.isLogin = false;
    }

    getDataLoginResult(loginRequest: LoginRequest): Observable<LoginResponse> {
        this.ngxSpinner.show();
        return this.http.post<LoginResponse>(this.urlApi, loginRequest, this.httpOptions)
        .pipe(
            map(data => { 
                this.ngxSpinner.hide(); 
                return data as LoginResponse;
             })
            );
    }
    checkLogin(userName, passWord) {
        const loginRequest = new LoginRequest();
        loginRequest.username = userName;
        loginRequest.password = passWord;
        loginRequest.appCode = "mbccs";
        loginRequest.prefix = "856";
        this.getDataLoginResult(loginRequest);
        //console.log("ok");
        return this.isLogin;
    }

    getIsLogin() {
        return this.isLogin;
    }

    /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}